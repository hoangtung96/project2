<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\accountcontroller;
use App\Http\Middleware\admin;
use App\Http\Controllers\admincontroller;
use App\Http\Controllers\categoriescontrller;
use App\Http\Controllers\itemcontroller;
use App\Models\item;
use App\Models\category;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// user
Route::resource('item','itemcontroller')->only(['index', 'show']);
Route::resource('categories','categoriescontrller')->only(['show']); 
Route::post('finditem','itemcontroller@find')->name('finditem');

// admin
Route::get('loginadmin','admincontroller@formLogin')->name('login.form');
Route::post('login','admincontroller@login')->name('login.admin');

Route::group(['middleware' => 'admin'], function() {
    Route::resource('item','itemcontroller')->only(['store', 'edit', 'update', 'destroy']); 

    Route::get('create','itemcontroller@create')->name('item.create');

    Route::get('update','itemcontroller@edit_delete')->name('item.update');
    Route::get('find_update','itemcontroller@find_update')->name('find.admin');
    Route::get('form_delete/{id}','itemcontroller@form_delete')->name('form.delete');
    Route::post('logout','admincontroller@logout')->name('admin.logout'); 
});
