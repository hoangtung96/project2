<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB; 

class Admin extends Model
{
    use HasFactory;
    protected $table = 'admin';
    public $timestamps = false ;
    protected $fillable = ['name', 'password'];

    public function login($request)
    {
        $input = $request->validated();
        $admin = DB::table('admin')->where([ ['name', $input['name']], ['password', $input['password']]])->get();
        if ($admin == '[]'){
            session()->flash('message','tên đăng nhập hặc mật khẩu không đúng ');
            return redirect()->route('login.form'); 
        }
        session_start();
        $_SESSION['id'] = $admin[0]->id;
        session()->flash('message','Logged in successfully');
        return redirect()->route('item.update');


          // if($remember) {
        //     setcookie ('user', $input['user'], time() + 300);
        //     setcookie ('password', $input['password'], time() + 300);
        //     setcookie ('id',$users[0]->id, time() + 300);
        // }

        // dd($admin);
    }

    public function logout()
    {
        session_start();
        if(isset($_SESSION['id'])) {
            session_unset();
            session_destroy(); 
        }
    }
}

