<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\item;

class category extends Model
{
    use HasFactory;
    protected $table = 'category';
    public $timestamps = false ;
    protected $fillable = ['name'];

    public function item()
    {
        return $this->HasMany('App\Models\item', 'categories_id', 'id');
    }

    public function item_Categories($id)
    {
        $categories = category::find($id);
        return $categories;
    }

    
}
