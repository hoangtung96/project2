<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\category;

class item extends Model
{
    use HasFactory;
    protected $table = 'item';
    public $timestamps = false ;
    protected $perPage = 5;
    protected $fillable = ['name','description', 'amount', 'price','image', 'categories_id'];

    public function allItems()
    {
        return DB::table('item')->paginate(15);
    }
    public function createItem($request)
    {
        $input = $request->validated();
        $input['image'] = $input['image']->getClientOriginalName();
        $image = $request->file('image');
        $image->move('image', $input['image']);
        $add = item::create($input);
        return session()->flash('message','create item successfully');
    }
    public function item()
    {
        return $this->BelongsTo('App\Models\category', 'categories_id', 'id');
    }

    public function showItem($id)
    {
        $item = item::find($id);
        return $item;
    } 

    public function findItem($request)
    {   
        $name = $request->validated();
        $items = item::query()
                ->where('name', 'LIKE', "%{$name['name']}%") 
                ->orWhere('description', 'LIKE', "%{$name['name']}%") 
                ->get();
        return $items;
    }

    public function delete_item($id)
    {   
        $item = item::find($id);
        $item->delete();
    }

    public function update_item($request, $id)
    {
        $input = $request->validated();
        $item = item::find($id);
        $item->name = $input['name'];
        $item->description = $input['description'];
        $item->price = $input['price'];
        $item->save();
    } 

    public function support($id)
    {
        $categories = item::find($id)->categories_id;
        $support = DB::table('item')
                    ->where('categories_id', '=', $categories)
                    ->get();
        $random =  $support->random(10)->all();

        return $random;
        
    }
    
}
