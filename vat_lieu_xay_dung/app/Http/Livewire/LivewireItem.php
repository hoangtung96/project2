<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\item;
use Livewire\Withpagination;
use Illuminate\Http\Request;

class LivewireItem extends Component
{
    public $searchTerm;
    public function render()
    {
        $this->data = item::where('name', 'like', '%'. $this->searchTerm .'%' )->paginate(10);;
        return view('livewire.livewire-item',['data'=>  $this->data]);
    }
}