<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\item;
use App\Models\Admin;
use App\Models\category;
use App\Http\Requests\itemcreaterequest;
use App\Http\Requests\find_itemrequest;
use App\Http\Requests\item_updaterequest;

class itemcontroller extends Controller
{
    public $item;
    public $admin;
    public function __construct(item $item, Admin $admin)
    {
        $this->item = $item;
        $this->admin = $admin;
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = $this->item->allItems();
        return view('item.all',['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(itemcreaterequest $request)
    {
        $items = $this->item->createItem($request);
        session()->flash('message','tạo sản phẩm thành công');
        return redirect()->route('item.create');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->item->showItem($id);
        $support = $this->item->support($id);
        return view('item.show',['item' => $item, 'support' => $support]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->item->showItem($id);
        return view('item.show_update',['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(item_updaterequest $request, $id)
    {
        $item = $this->item->update_item($request, $id);
        session()->flash('message','cập nhật thàng công');
        return redirect()->route('item.update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->item->delete_item($id);
        session()->flash('message','đã xóa thành công');
        return redirect()->route('item.update');
    }

    //build

    /**
     * product search by id
     *
     * @param find_itemrequest
     * @return \Illuminate\Http\Response
     */
    public function find(find_itemrequest $request)
    {   
        $item = $this->item->findItem($request);
        return view('item.find',['data' => $item]);
    }

    /**
     * load form item delete
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_delete()
    {   
        $data = $this->item->allItems();
        return view('item.update_delete',['data' => $data]);
    }

    /**
     * product search by name
     *
     * @param find_itemrequest
     * @return \Illuminate\Http\Response
     */
    public function find_update(find_itemrequest $request)
    {   
        $item = $this->item->findItem($request);
        return view('item.find_update',['data' => $item]);
    }

    /**
     * product delete by id
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function form_delete($id)
    {   
        $item = $this->item->showItem($id);
        return view('item.delete',['item' => $item]);
    }
}
