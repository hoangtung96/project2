<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\category;

class categoriescontrller extends Controller
{
    public $category;
    public function __construct(category $category)
    {
        $this->category = $category;
    } 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = $this->category->item_Categories($id);
        return view('item.categories', ['categories' => $categories]);
        
    }
}
