<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\adminrequest;
use App\Models\Admin;
use App\Models\item;

class admincontroller extends Controller
{
    public $admin;
    public $item;
    public function __construct(Admin $admin,item $item)
    {
        $this->admin = $admin;
        $this->item = $item;
    } 

    public function login(adminrequest $request)
    {
        return $this->admin->login($request);
    }

    public function formLogin()
    {
        return view('admin.login');
    }

    public function logout()
    {
        $this->admin->logout();
        return redirect()->route('login.form');
    }
}
