<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        session_start();
        if (isset($_SESSION['id'])) {
            return $next($request);
            // return redirect()->route('item.update');
        } else  {
            return redirect()->route('login.form');
        }
    }
}
