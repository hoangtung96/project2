
<header>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<title>@yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}" />
</header>
<style type="text/css">
    /* home */
    .header_footer1{
        width: 2000px;
        height: 250px;
        padding-top: 20px;
        text-align: center;
        background: #008080	;
    }
    .header_footer2{
        width: 100%;
        height: 200px;
        text-align: center;
        background: #008080;
        padding-top: 30px;
    }
    .header{
        /* width: 1200px; */
        width: 100%;
        height: 100px;
        display: flex;
        margin-left:200px ;
        text-align: center; 
    }
    .header_child{
       margin-left: 50px;  
    }

    /* conten */
    .conten{
        width: 100%;
        height: 770px;  
        text-align: center; 
    }
    }
    .conten_child{
        width: 400px;
        height: 350px; 
        text-align: center; 
    }
    .wrapper {
        display: grid;
        grid-template-columns: 400px 400px 400px 400px 400px;
        grid-template-rows: 450px 450px 450px;
        text-align: center;
    }

    .wrapper_copy {
        display: grid;
        grid-template-columns: 400px 400px 400px 400px 400px;
        grid-template-rows: 450px 450px;
        text-align: center;
    }

    .show_wrapper {
        display: grid;
        grid-template-columns: 500px 1000px 100px 500px ;
        grid-template-rows: 800px;
        padding-left: 0px;
        /* text-align: center;  */
    }

    .show_child {
        width: 500px;
        height: 500px;  
        text-align: center;
    }
    .show_child {
        display: grid;
        grid-template-columns: 500px;
        grid-template-rows: 150px 150px 150px 150px 150px;
        text-align: center; 
    }
    .tu_van{
        display: flex;
        text-align: center; 
    }

    .show_grandchildren {
        display: grid;
        grid-template-columns: 1000px;
        grid-template-rows: 100px 100px 100px 100px 230px 150px;
        text-align: center; 
    }
    .form{
        display: flex;
    }

    .form_1{
        padding-left: 20px
    }

    .login {
        display: grid;
        grid-template-columns: 1000px 1000px 1000px ;
        grid-template-rows: 800px;
        padding-left: 0px;
        display: flex;
    }

    .login_child {
        width: 700px;
        height: 700px;
    }

    .update_delete{
        display: flex;
        text-align: center;
        margin-left: 150px;
        /* padding: 50px; */
    }

    .next{
        text-align: center;
        width: 100%;
        height: 20px;
    }

    </style>
    @livewireStyles
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</header>
<body>
    <div class="header_footer1">
        <div>
            <div>
                <img src="/image/logo.jpg"  height="100px" width="100px" /></td>
                <h2>CHUYÊN VỀ  VẬT LIỆU XÂY DỰNG VÀ CÁC THIẾT BỊ VỆ SINH</h2>
            <div>
        </div>
        <div class="header">
            <div class="header_child"><a href="http://localhost:8000/item"><b><h4 style="color:red">TRANG CHỦ</h4></b></a></div>
            <div class="header_child"><a href="http://localhost:8000/categories/1"><b><h4 style="color:red">GẠCH LỖ </h4></b></a></div>
            <div class="header_child"><a href="http://localhost:8000/categories/2"><b><h4 style="color:red">XI MĂNG</h4></b></a></div>
            <div class="header_child"><a href="http://localhost:8000/categories/3"><b><h4 style="color:red">THÉP</h4></b></a></div>
            <div class="header_child"><a href="http://localhost:8000/categories/4"><b><h4 style="color:red">GACH TRANG TRÍ</h4></b></a></div>
            <div class="header_child"><a href="http://localhost:8000/categories/5"><b><h4 style="color:red">THIÊT BỊ VỆ SINH</h4></b></a></div>
            <div class="header_child"><h4>LIÊN HỆ : 0123456789</h4></div>
            <div class="header_child">
                <form action ="{{ route('finditem')}}" placeholder="Search" method ='post' class = 'text-center'>
                    @csrf
                    <div class="form">
                        <div class="form_1">
                            <input type = 'text' name = 'name'>
                        </div>
                        <div class="form_1">
                            <button type = 'submit' name = 'submit' class = 'btn btn-primary'>TÌM KIẾM</button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 

    @yield('content')

    <div class="header_footer2" >
        <div>
            <h2>CÔNG TY TRÁCH NHIỆM HỮU HẠN VIỆT NAM</h2>
            <h4>LIÊN HỆ 0123456789</h2>
        </div>
        <div>
            <h3>ĐỊA CHỈ</h3>
            <h5>Số 307- Đường 72 Cũ La Dương - Hà Đông - Hà Nội Hotline: 0978956655 - 0946862099 </h5>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script> 
    @livewireScripts 
</body>
