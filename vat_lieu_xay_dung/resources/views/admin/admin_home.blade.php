
<header>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<title>@yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}" />
</header>
    <style type="text/css">
    .header_footer1{
        width: 2000px;
        height: 220px;
        padding-top: 20px;
        text-align: center;
        background: #008080	;
    }

    .header_footer2{
        width: 100%;
        height: 200px;
        text-align: center;
        background: #008080;
        padding-top: 30px;
    }
    .sesion{
        width: 100%;
        height: 200px;
        margin-top:50px;
    }

    .show{
        display: flex;
        padding-left:0px;
    }

    .show1{
        margin-left:0px;
    }

    .conten{
        width: 100%;
        height: 700px;
        margin-top: 50px;
    }

    .control{
        margin-top: 40px;
    }

    .image_input{
        text-align: center;
    }

    .update{
        width: 100%;
        height: 650px;
    }

    .update1{
        width: 100%;
        height: 30px;
        margin-top: 50px;
    }
    .update2{
        width: 100%;
        height: 450px;
        margin-top: 50px;
    }

    .update3{
        width: 1900;
        height: 150px;
        margin-top: 50px;
    }
    .headertable1{
        width: 200;
        height: 50px;
        text-align: center;
    }
    .headertable2{
        width: 1400;
        height: 50px;
        text-align: center;
    }
    .headertable3{
        width: 150;
        height: 50px;
        text-align: center;
    }

    .tbody1{
        width: 1400px;
        height: 100px;
        text-align: center;
    }

    .form_delete{
        width: 1500;
        height: 500px;
        margin-left: 400px;
        padding-top: 100px;
        display: flex;
    }
    .form_delete2{
        margin-left: 400px;
        
    }

    </style>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</header>
<body>
    <div class="header_footer1">
        <div>
            <div>
                <img src="/image/logo.jpg"  height="100px" width="100px" /></td>
                <h2>CÔNG TY TRÁCH NHIỆM HỮU HẠN VIỆT NAM</h2>
            <div>
            <div>
                <form action="{{route('admin.logout')}}" method="POST" >
                @csrf
                    <button type = 'submit' name='submit' class = 'btn btn-primary'>ĐĂNG XUẤT</button>
                </form>
            <div>
        </div>
    </div> 

    @yield('content')

    <div class="header_footer2" >
        <div>
            <h2>CÔNG TY TRÁCH NHIỆM HỮU HẠN VIỆT NAM</h2>
            <h4>LIÊN HỆ 0123456789</h2>
        </div>
        <div>
            <h3>ĐỊA CHỈ</h3>
            <h5>Số 307- Đường 72 Cũ La Dương - Hà Đông - Hà Nội Hotline: 0978956655 - 0946862099 </h5>
        </div>
    </div>
</body>
