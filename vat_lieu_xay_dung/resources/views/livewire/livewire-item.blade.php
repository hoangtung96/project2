<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Search</h2>
            <div class="form-group">
                <input type="text"  class="form-control" placeholder="Search" wire:model="searchTerm" />
                <table>
                    <thead>
                        <tr>
                            <th id = "headertable">STT</th>
                            <th id = "headertable">ID</th>
                            <th id = "headertable">TÊN BÀI ĐĂNG</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dt)
                            <tr id ="data">
                                <td>{{$dt->id}}</td>
                                <td>{{$dt->name}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    <div>
<div>
     
