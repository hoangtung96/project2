@extends('admin.admin_home')
@section('title', 'create item')
@section('content')
  <div class="conten">
        @if(session()->has('message'))
            <div style='color:blue'>{{session('message')}}</div>
        @endif
        <a href="http://localhost:8000/update"><b><h2>TRANG CHỦ</h2></b></a>
        <form action ="{{route('item.store')}}"  class = 'text-center'  enctype="multipart/form-data" method ='POST'>
            @csrf
            <div >
                <h3>TẠO SẢN PHẨM</h3>
            </div>
            <div  class="control">
                <lable for = 'name' >TÊN SẢN PHẨM</lable><br/>
                <input type = textbox name = 'name' id = 'name' size="50">
            </div>
            <div  class="control">
                <lable for = 'description'>MIÊU TẢ</lable><br/>
                <textarea rows="4" cols="50" name="description" > </textarea>
            </div>
            <div class="control">
                <lable for='price'>GIÁ BÁN</lable><br/>
                <input type = 'textbox'  name = 'price' id='price' size="50">
            </div> 
            <div  class="control">
                <div class="image_input">
                    <lable for='price'>HÌNH ẢNH</lable><br/>
                    <input type="file" name="image" id = 'image' class ="image_input"> 
                </div>
            </div> 
            <div class="control">
                <input type="radio" id="contactChoice1"
                    name="categories_id" value="1" checked>
                <label for="contactChoice1">GẠCH LỖ</label>
                <input type="radio" id="contactChoice2"
                    name="categories_id" value="2">
                <label for="contactChoice2">XI MĂNG</label>
                <input type="radio" id="contactChoice3"
                    name="categories_id" value="3">
                <label for="contactChoice3">THÉP</label>
                <input type="radio" id="contactChoice3"
                    name="categories_id" value="4">
                <label for="contactChoice3">GẠCH TRANG TRÍ</label>
                <input type="radio" id="contactChoice3"
                    name="categories_id" value="5">
                <label for="contactChoice3">THIẾT BỊ VỆ SINH</label>
            </div>
            <div class="control">
                <button type = 'submit' name = 'submit' class = 'btn btn-primary'>TẠO SẢN PHẨM</button> 
            </div>
        </form> 
  </div>
@endsection
