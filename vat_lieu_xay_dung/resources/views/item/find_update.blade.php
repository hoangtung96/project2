@extends('admin.admin_home')
@section('title', 'all items')
@section('content')
    <div id ="content"> 
    @csrf
        <div class="update1">
            <h1>TÌM KIẾM SẢN PHẨM</h1>
            <a href="http://localhost:8000/update"><b><h3>TRANG CHỦ</h3></b></a>
        </div>  
        <div id="table" > 
            <table>
                <thead>
                    <tr>
                        <th id = "headertable">STT</th>
                        <th id = "headertable">TÊN MẶT HÀNG</th>
                        <th id = "headertable">ĐẶC ĐIỂM </th>
                        <th id = "headertable">GIÁ</th>
                        <th id = "headertable">HÌNH ẢNH</th>
                        <th id = "headertable"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $item)
                        <tr id ="data">
                            <td>{{$key + 1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->price}}</td>
                            <td><img src="image/{{$item->image}}"  height="100px" width="100px" /></td>
                            <td>
                                <div padding: 10px>
                                    <form action="{{route('form.delete', $item->id)}}">
                                    @csrf
                                        <button type = 'submit' name='submit' class = 'btn btn-primary'>XÓA</button> 
                                    </form>
                                    <form action ="{{route('item.edit', $item->id)}}">
                                    @csrf
                                        <button type = 'submit' name='submit' class = 'btn btn-primary' >SỬA</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection 