@extends('admin.admin_home')
@section('title', 'edit item')
@section('content')
    <div class="update" > 
    @csrf
        <div class="update1">
            <h1>THÔNG TIN SẢN PHẨM</h1>
            <a href="http://localhost:8000/update"><b><h2>TRANG CHỦ</h2></b></a>
        </div>
        <div class="update2">
            <form method="POST" action="http://localhost:8000/item/{{$item->id}}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
                <div class="update3">
                    <table>
                        <thead>
                            <tr>
                                <th class="headertable1">TÊN MẶT HÀNG</th>
                                <th class="headertable2">ĐẶC ĐIỂM </th>
                                <th class="headertable3">GIÁ</th>
                                <th class="headertable1">CHỦNG LOẠI</th>
                                <th class="headertable1">HÌNH ẢNH</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="headertable1">{{$item->name}}</div></td>
                                <td class="tbody1">{{$item->description}}</div></td>
                                <td class="headertable3">{{$item->price}}</div></td>
                                <td class="headertable1">{{$item->item->name}}</div></td>
                                <td class="headertable1"><img src="/image/{{$item->image}}" height="100px" width="100px"/></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <div  class="control">
                        <lable for = 'name' >NHẬP TÊM MỚI SẢN PHẨM</lable><br/>
                        <input type = textbox name = 'name' id = 'name' size="50" value = "{{$item->name}}">
                    </div>
                    <div  class="control">
                        <lable for = 'description'>MIÊU TẢ</lable><br/>
                        <textarea rows="4" cols="50" name="description">{{$item->description}}</textarea>
                    </div>
                    <div class="control">
                        <lable for='price'>GIÁ MỚI</lable><br/>
                        <input type = 'textbox'  name = 'price' id='price' size="50"  value = "{{$item->price}}">
                    </div> 
                </div>
                <div>
                    <button type = 'submit' name='submit' class = 'btn btn-primary'>CẬP NHẬT</button>
                </div>
            </form>
        </div>
    </div>
@endsection