
@extends('admin.admin_home')
@section('title', 'all items')
@section('content')
    <div > 
        <div class="sesion">
            <div>
                <h1>TẤT CẢ SẢN PHẨM</h1>
            </div>
            @if(session()->has('message'))
                <div style='color:blue'>{{session('message')}}</div>
            @endif
            <div class="show" >
                <div class="show1" >
                    <form action ="{{ route('item.create')}}">
                    @csrf
                        <button type = 'submit' name='submit' class = 'btn btn-primary'>THÊM SẢN PHẨM</button> 
                    </form>
                </div>
            </div>
                <form action ="{{ route('find.admin')}}" placeholder="Search" method ='get' class = 'text-center'>
                    @csrf
                    <div >
                        <div >
                            <input type = 'text' name = 'name'>
                        </div>
                        <div >
                            <button type = 'submit' name = 'submit' class = 'btn btn-primary'>TÌM KIẾM</button> 
                        </div>
                    </div>
                </form>   
            </div>
        </div>
        <div> 
            <table>
                <thead>
                    <tr>
                        <th id = "headertable">STT</th>
                        <th id = "headertable">TÊN MẶT HÀNG</th>
                        <th id = "headertable">ĐẶC ĐIỂM </th>
                        <th id = "headertable">GIÁ</th>
                        <th id = "headertable">HÌNH ẢNH</th>
                        <th id = "headertable"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $item)
                        <tr id ="data">
                            <td>{{$key + 1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->price}}</td>
                            <td><img src="/image/{{$item->image}}"  height="100px" width="100px" /></td>
                            <td>
                                <div padding: 10px>
                                    <form action="{{route('form.delete', $item->id)}}">
                                    @csrf
                                        <button type = 'submit' name='submit' class = 'btn btn-primary'>XÓA</button> 
                                    </form>
                                    <form action="{{route('item.edit', $item->id)}}">
                                    @csrf
                                        <button type = 'submit' name='submit' class = 'btn btn-primary' >SỬA</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div >
            <spam>
                {{ $data->links() }}
            </spam>
        </div>
    </div>
@endsection 