@extends('home')
@section('title', 'show item')
@section('content')
    <div class="show_wrapper">
        <div> 
            <img src="/image/{{$item->image}}" height="400" width="400" />
        </div>
        <div class="show_grandchildren">
            <div><h4>TÊN SẢN PHẨM : {{$item->name}}<h4></div>
            <div><h4>CHỦNG LOẠI : {{$item->item->name}}<h4></div>
            <div><h4>GIÁ SẢN PHẨM : {{$item->price}} VND<h4></div>
            <div><h4>LIỆN HỆ SỐ ĐIỆN THOẠI : 0123456789 <h4></div>
            <div> 
                <img src="/image/{{$item->image}}" height="200" width="200"/>
            </div>
            <div><h4>ĐẶC ĐIỂM SẢN PHẨM : {{$item->description}}<h4></div>
        </div>
        <div>
        </div>
        <div class="show_child">
            <div class="tu_van">
                <img src="/image/tu-van-san-pham.png" height="75" width="75" />
                <div>
                    <h4>TƯ VẤN 24/24</h4>
                    <h4>Hotline: 0978 956 655</h4>  
                </div>
            </div>
            <div class="tu_van">
                <img src="/image/giao-nhan.png" height="75" width="75" />
                <div>
                    <h4>Giao hàng toàn quốc</h4>
                    <h4>Vận chuyển 63 tỉnh thành</h4> 
                </div>
            </div>
            <div class="tu_van">
                <img src="/image/icon-uy-tin-7933-5903.png" height="75" width="75" />
                <div>
                    <h4>BẢO HÀNH TẬN NƠI</h4>
                    <h4>Nhanh chóng và đảm bảo</h4> 
                </div>
            </div>
            <div class="tu_van">
                <img src="/image/ho-tro-thi-cong.png" height="75" width="75" />
                <div>
                    <h4>Tư vấn thiết kế</h4>
                    <h4>Tư vấn bản vẽ thiết kế</h4> 
                </div>
            </div>
            <div class="tu_van">
                <img src="/image/gia-hang.png" height="150" width="250" />
            </div>  
        </div>
    </div>
    ***********************************************************************************
    <div>
        <h3> SẢN PHẨM BẠN CÓ THỂ QUAN TÂM </h3>
    </div>
    <div>
    <div class="wrapper_copy">
        @foreach($support as $dt)
            <div class="conten_child">
                <a href="http://localhost:8000/item/{{$dt->id}}"> <img src="/image/{{$dt->image}}"  height="250px" width="250px" /></a>
                <h4 style="color:red"><strike> ({{$dt->price+($dt->price)*0.2}})VND</strike><h4>
                <h4>giá:{{$dt->price}} VND<h4>
                <a href="http://localhost:8000/item/{{$dt->id}}"><h4><b>{{$dt->name}}</b></h4></a>
            </div>
        @endforeach
    </div>
    </div>

@endsection 