<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\item;

class itemseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        item::factory()
            ->count(25)
            ->create();
    }
}
